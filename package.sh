#!/usr/bin/env bash
set -euo pipefail

version=${1:?Please provide the version as the first argument}
dist_dir=${2:-./dist}
out_dir="${dist_dir}/${version}"
build_tmp_dir=${3:-./build/tmp}
inner_folder_name=${4:-the_last_nomad}

# Zip build
zip_build () {
    target=$1

    # Ensure fresh build dir
    [ -d "${build_tmp_dir}" ] && rm -rf "${build_tmp_dir}"
    mkdir -p "${build_tmp_dir}"

    # Create zip file
    cp -r "./build/${target}" "${build_tmp_dir}/${inner_folder_name}/"
    pushd "${build_tmp_dir}"
    zip -r "./game.zip" ${inner_folder_name}
    popd
    cp "${build_tmp_dir}/game.zip" "${out_dir}/${target}.zip"
}

# Ensure output dir does not exist and create it
if [ -d "${out_dir}" ]; then
    echo "Output dir already exists!"
    exit 1
fi
mkdir -p "${out_dir}"

# Create platform packages
zip_build linux
zip_build windows
cp "build/mac/${inner_folder_name}.zip" "${out_dir}/mac.zip"

# Cleanup
[ -d "${build_tmp_dir}" ] && rm -rf "${build_tmp_dir}"

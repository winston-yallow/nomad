extends KinematicBody


enum STATE { BROKEN, WAITING, FOLLOWING }

var nav: Navigation
var player: Player

var detected_bodies := []

var was_near_target_last_frame := false

var current_state: int = STATE.BROKEN
var velocity := Vector3()
var desired_velocity := Vector3()
export var player_offset := Vector3(0.0, 0.0, 0.0)
export var social_distancing := 2.75
export var max_speed := 6.8
export var max_rotation_speed := 4.0
export var velocity_lerp := 18.0
export var gravity := 1.0

var curve := Curve3D.new()
var last_player_pos: Vector3
var xz_plane := Vector3(1, 0, 1)
export var lookahead := 0.75

export var eye_speed := 1.75
export var eye_dist := 4.0

onready var beacon: Beacon = $Beacon
onready var eye: Spatial = $EyePivot

onready var rotation_player := $RotationPlayer
onready var movement_player := $MovementPlayer
onready var droid_player := $DroidPlayer


func _ready() -> void:
    # Not very clean, but prevents an error from being thrown if the
    # navigation does not exist in the expected place.
    var scene := get_tree().current_scene
    if scene.has_node('Navigation'):
        nav = scene.get_node('Navigation')
    else:
        nav = Navigation.new()
        scene.call_deferred('add_child', nav)
        push_warning('Navigation must exist in the current_scene')
    
    set_beacon_command('repair', 'repair', 2)
    
    # warning-ignore:return_value_discarded
    $Detector.connect('body_entered', self, 'on_body_entered')
    # warning-ignore:return_value_discarded
    $Detector.connect('body_exited', self, 'on_body_exited')


func _process(delta: float) -> void:
    if not is_instance_valid(player):
        return  # Nothing to do if there is no player assigned (robot still broken)
    
    # Decide what to look at
    var lookat_pos: Vector3
    if player.global_transform.origin.distance_to(global_transform.origin) < eye_dist:
        lookat_pos = player.global_transform.origin + Vector3(0.0, 0.5, 0.0)
    else:
        lookat_pos = eye.global_transform.origin - global_transform.basis.z
    
    # Smoothly look to target with spherical linear interpolation (slerp)
    var current_basis := eye.global_transform.basis
    var target_basis := eye.global_transform.looking_at(lookat_pos, Vector3.UP).basis
    eye.global_transform.basis = current_basis.slerp(target_basis, delta * eye_dist)


func _physics_process(delta: float) -> void:
    desired_velocity.x = 0
    desired_velocity.z = 0
    
    if current_state == STATE.FOLLOWING:
        
        if not is_close_to_target():
            
            var floor_pos := global_transform.origin * xz_plane
            var offset := curve.get_closest_offset(global_transform.origin)
            var target_pos := curve.interpolate_baked(offset + lookahead) * xz_plane
            var forward := -global_transform.basis.z
            var target_dir := (target_pos - floor_pos).normalized()
            var angle := forward.angle_to(target_dir)
            var direction := sign(forward.cross(target_dir).y)
            var phi := min(delta * max_rotation_speed, angle)
            rotate_y(phi * direction)
            rotation_player.audible = phi < angle
            
            var movement_angle := TAU / 8.0  # is allowed to move if target inside 45°
            var weight := clamp(angle / movement_angle, 0.0, 1.0)
            var target_speed: float = lerp(max_speed, 0.0, weight)
            if not detected_bodies:
                desired_velocity = forward * target_speed
            movement_player.audible = weight < 0.95 and not detected_bodies
            
            was_near_target_last_frame = false
            
        else:
            
            if not was_near_target_last_frame and not droid_player.playing:
                droid_player.play()
            
            was_near_target_last_frame = true
            rotation_player.audible = false
            movement_player.audible = false
        
        var last_dist := last_player_pos.distance_to(player.global_transform.origin)
        if last_dist > social_distancing:
            recalc_curve_to(player)
        
    else:
        rotation_player.audible = false
        movement_player.audible = false
    
    # Apply forces
    velocity.x = lerp(velocity.x, desired_velocity.x, velocity_lerp * delta)
    velocity.y -= gravity
    velocity.z = lerp(velocity.z, desired_velocity.z, velocity_lerp * delta)
    
    # Move according to current velocity
    velocity = move_and_slide(velocity, Vector3.UP, true)


func is_close_to_target() -> bool:
    
    # Just pretend we reached the target if it is not a valid curve
    if curve.get_point_count() <= 1.0:
        return true
    
    var closest_offset := curve.get_closest_offset(global_transform.origin)
    var remaining := curve.get_baked_length() - closest_offset
    return remaining < social_distancing


func recalc_curve_to(p: Player) -> void:
    var start := global_transform.origin
    last_player_pos = p.global_transform.origin
    var target_displacement = p.global_transform.basis.xform(player_offset)
    var path := nav.get_simple_path(start, last_player_pos + target_displacement)
    curve.clear_points()
    for point in path:
        curve.add_point(point)
    print('Recalced')


func set_beacon_command(title: String, method: String, dist: float, args := []) -> void:
    beacon.clear_cmds()
    beacon.register_cmd(Command.new(title, self, method, dist, args))


func set_state(new_state: int) -> void:
    current_state = new_state
    match current_state:
        STATE.FOLLOWING:
            set_beacon_command('disable following', 'wait', 100)
        STATE.WAITING:
            set_beacon_command('enable following', 'navigate_to', 100)


func repair(p: Player) -> void:
    if current_state == STATE.BROKEN:
        player = p
        set_state(STATE.WAITING)


func rename(title: String, description: String) -> void:
    beacon.title = title
    beacon.description = description


func navigate_to(p: Player) -> void:
    recalc_curve_to(p)
    set_state(STATE.FOLLOWING)


func wait(_p: Player) -> void:
    set_state(STATE.WAITING)


func on_body_entered(other: Spatial) -> void:
    if not other in detected_bodies and not other == self:
        detected_bodies.append(other)


func on_body_exited(other: Spatial) -> void:
    while other in detected_bodies:
        detected_bodies.erase(other)

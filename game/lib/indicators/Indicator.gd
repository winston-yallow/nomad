class_name Indicator
extends Area


enum QUALITY {
    GOOD = -1,
    NEUTRAL = 0,
    BAD = 1
}


export(QUALITY) var radiation: int = 0
export(QUALITY) var pollution: int = 0

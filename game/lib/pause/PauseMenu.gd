extends CanvasLayer


onready var menu: Control = $Menu


func _ready() -> void:
    _set_state(false)


func _input(event: InputEvent) -> void:
    if event.is_action_pressed('ui_cancel') and not Cutscene.is_playing:
        _set_state(not get_tree().paused)


func resume_game() -> void:
    _set_state(false)


func _set_state(paused: bool) -> void:
    get_tree().paused = paused
    menu.visible = paused

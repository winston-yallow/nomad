class_name PlayerTrigger
extends Area


signal triggered()


func _ready() -> void:
    # warning-ignore:return_value_discarded
    connect('body_entered', self, 'on_body_entered')


func on_body_entered(other: Spatial) -> void:
    if other is Player:
        emit_signal('triggered')

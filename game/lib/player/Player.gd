class_name Player
extends KinematicBody


signal beacon_registered(beacon)
signal beacon_unregistered(beacon)

# warning-ignore:unused_signal
signal charging_started()
signal charging_ended()

# warning-ignore:unused_signal
signal stranded()


enum STATUS { IDLE, MOVE, ROTATE, CHARGING }


var current_status: int = STATUS.IDLE
var moving_forward := true

var battery_level := 100.0
var can_charge := false
export var charging_speed := 12.0
export var discharging_speed := 0.7
export var charging_brake_lerp := 7.5

var indicators := []
var radiation := 0.0
var pollution := 0.0
var radiation_next_random := 0.0
var pollution_next_random := 0.0
var radiation_current_random := 0.0
var pollution_current_random := 0.0
export var quality_default := 50.0
export var quality_offset := 40.0
export var quality_rng_range := Vector2(-8, 8)
export var quality_rng_interval := Vector2(0.5, 1.75)

var reports := []
export var min_report_distance := 17.0

var current_rotation_speed := 0.0
export var rotation_speed_max := 1.3
export var rotation_speed_lerp := 8.0

var velocity := Vector3()
export var gravity := 1.0
export var speed_max := 7.0
export var speed_lerp := 15.0

export var action_switch_time := 0.2
onready var time_since_rotation := action_switch_time
onready var time_since_movement := action_switch_time

onready var detector: Area = $Detector

onready var movement_player := $MovementPlayer
onready var rotation_player := $RotationPlayer

export var deactivated := false


func _ready() -> void:
    randomize()
    # warning-ignore:return_value_discarded
    detector.connect('area_entered', self, 'on_area_entered')
    # warning-ignore:return_value_discarded
    detector.connect('area_exited', self, 'on_area_exited')
    
    if deactivated:
        set_process(false)
        set_physics_process(false)


func _process(delta: float) -> void:
    
    # Get air indicator sums
    var r_sum := 0
    var p_sum := 0
    for i in indicators:
        r_sum += i.radiation
        p_sum += i.pollution
    
    # Calculate the raw target based on air indicator sums
    var r_target := quality_default + (sign(r_sum) * quality_offset)
    var p_target := quality_default + (sign(p_sum) * quality_offset)
    
    # Update randomness
    radiation_next_random -= delta
    pollution_next_random -= delta
    if radiation_next_random <= 0.0:
        radiation_current_random = lerp(quality_rng_range.x, quality_rng_range.y, randf())
        radiation_next_random = lerp(quality_rng_interval.x, quality_rng_interval.y, randf())
    if pollution_next_random <= 0.0:
        pollution_current_random = lerp(quality_rng_range.x, quality_rng_range.y, randf())
        pollution_next_random = lerp(quality_rng_interval.x, quality_rng_interval.y, randf())
    
    # Lerp radiation towards target
    radiation = lerp(radiation, r_target + radiation_current_random, delta)
    pollution = lerp(pollution, p_target + pollution_current_random, delta)


func _physics_process(delta: float) -> void:
    
    time_since_rotation += delta
    time_since_movement += delta
    
    if current_status == STATUS.CHARGING:
        apply_charging(delta)
    
    else:
        apply_rotation(delta)
        apply_movement(delta)
        
        # Reset status if timers reached action threshold
        var rotation_stopped := time_since_rotation > action_switch_time
        var movement_stopped := time_since_movement > action_switch_time
        if rotation_stopped and movement_stopped:
            current_status = STATUS.IDLE
        
        # Discharge battery and force status to CHARGING if empty
        battery_level -= discharging_speed * delta
        if battery_level <= 0.0:
            var state := get_world().direct_space_state
            can_charge = not is_obstructed(state)
            battery_level = 0.0
            current_status = STATUS.CHARGING
            reset_velocity()
            emit_signal('charging_started' if can_charge else 'stranded')
        
        movement_player.audible = current_status == STATUS.MOVE
        rotation_player.audible = current_status == STATUS.ROTATE


func distance_to(other: Spatial) -> float:
    return global_transform.origin.distance_to(other.global_transform.origin)


func apply_charging(delta: float) -> void:
    
    if can_charge:
        battery_level += charging_speed * delta
        if battery_level >= 100.0:
            battery_level = 100.0
            current_status = STATUS.IDLE
            emit_signal('charging_ended')
    
    # Apply gravity and brakes
    velocity.x = lerp(velocity.x, 0.0, delta * charging_brake_lerp)
    velocity.y -= gravity
    velocity.z = lerp(velocity.z, 0.0, delta * charging_brake_lerp)
    velocity = move_and_slide(velocity, Vector3.UP, true)


func abort_charging() -> void:
    current_status = STATUS.IDLE
    emit_signal('charging_ended')


func apply_rotation(delta: float) -> void:
    
    # Get user input
    var desired_rotation_speed := 0.0
    if time_since_movement > action_switch_time:
        desired_rotation_speed += Input.get_action_strength('rotate_left')
        desired_rotation_speed -= Input.get_action_strength('rotate_right')
        desired_rotation_speed *= rotation_speed_max
    
    # Lerp the actual rotation speed towards the desired speed
    current_rotation_speed = lerp(
        current_rotation_speed,
        desired_rotation_speed,
        delta * rotation_speed_lerp
    )
    
    # Apply the actual rotation speed
    rotate_y(current_rotation_speed * delta)
    
    # Reset timer if the user wanted to rotate in this frame
    if desired_rotation_speed != 0.0:
        current_status = STATUS.ROTATE
        time_since_rotation = 0.0


func apply_movement(delta: float) -> void:
    var forward := global_transform.basis.z
    
    # Get current ground speed based on velocity (and velocity direction)
    var ground_velocity := velocity * Vector3(1, 0, 1)
    var current_speed := ground_velocity.length()
    if ground_velocity.angle_to(forward) > TAU/4.0:
        current_speed *= -1.0
    
    # Get desired speed
    var desired_speed := 0.0
    if time_since_rotation > action_switch_time:
        desired_speed -= Input.get_action_strength('move_forward')
        desired_speed += Input.get_action_strength('move_backward')
        desired_speed *= speed_max
    
    # Calculate new ground velocity
    current_speed = lerp(current_speed, desired_speed, delta * speed_lerp)
    ground_velocity = forward * current_speed
    
    # Apply movements to velocity
    velocity.x = ground_velocity.x
    velocity.y -= gravity
    velocity.z = ground_velocity.z
    
    velocity = move_and_slide(velocity, Vector3.UP, true)
    
    # Reset timer if the user wanted to move in this frame
    if desired_speed != 0.0:
        current_status = STATUS.MOVE
        time_since_movement = 0.0
        moving_forward = desired_speed < 0.0  # negative speed is forward


func is_obstructed(state: PhysicsDirectSpaceState) -> bool:
    var from := global_transform.origin
    var to := from + (Vector3.UP * 100.0)
    var result := state.intersect_ray(from, to, [self], 0b1)
    return not result.empty()


func can_report() -> bool:
    for pos in reports:
        if pos.distance_to(global_transform.origin) < min_report_distance:
            return false
    return radiation < 25 and pollution < 25


func reset_velocity() -> void:
    current_rotation_speed = 0.0
    velocity.x = 0.0
    velocity.z = 0.0
    rotation_player.volume_db = -80.0
    movement_player.volume_db = -80.0


func create_report() -> void:
    reports.append(global_transform.origin)


func on_area_entered(other: Area) -> void:
    if not other in indicators:
        indicators.append(other)


func on_area_exited(other: Area) -> void:
    while other in indicators:
        indicators.erase(other)


func register_beacon(beacon: Beacon) -> void:
    emit_signal('beacon_registered', beacon)


func unregister_beacon(beacon: Beacon) -> void:
    emit_signal('beacon_unregistered', beacon)

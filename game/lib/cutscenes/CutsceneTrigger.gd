class_name CutsceneTrigger
extends Area


signal triggered()


export var cutscene: PackedScene


func _ready() -> void:
    # warning-ignore:return_value_discarded
    connect('body_entered', self, 'on_body_entered')


func on_body_entered(other: Spatial) -> void:
    if other is Player:
        queue_free()
        emit_signal('triggered')
        other.reset_velocity()
        Cutscene.play(cutscene.instance())

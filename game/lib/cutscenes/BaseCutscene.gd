extends Spatial


func end_cutscene() -> void:
    if get_tree().current_scene == self:
        # This allows playing the cutscene when run directly for testing
        # warning-ignore:return_value_discarded
        get_tree().reload_current_scene()
    else:
        Cutscene.stop()

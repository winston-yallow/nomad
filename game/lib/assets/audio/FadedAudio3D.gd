extends AudioStreamPlayer3D


var diff: float
var factor: float
var current_ratio := 1.0
export var audible := false
export var time := 0.5
export var fade_max_db := 0.0
export var fade_min_db := -80.0


func _ready() -> void:
    diff = fade_max_db - fade_min_db
    factor = 1.0 / time
    current_ratio = 1 if audible else 0


func _process(delta: float) -> void:
    
    var direction := 1.0 if audible else -1.0
    current_ratio = clamp(current_ratio + (delta * direction * factor), 0.0, 1.0)
    unit_db = fade_min_db + (current_ratio * diff)

extends Spatial


export var lod_0_node := NodePath('LOD_0')
export var lod_1_node := NodePath('LOD_1')
export var lod_2_node := NodePath('LOD_2')

export var lod_1_dist := 30.0
export var lod_2_dist := 60.0

onready var lod_0: Spatial = get_node(lod_0_node)
onready var lod_1: Spatial = get_node(lod_1_node)
onready var lod_2: Spatial = get_node(lod_2_node)

onready var cam := get_viewport().get_camera()


func _process(_delta: float) -> void:
    var dist := cam.global_transform.origin.distance_to(global_transform.origin)
    lod_0.visible = dist <= lod_1_dist
    lod_1.visible = dist <= lod_2_dist and dist > lod_1_dist
    lod_2.visible = dist > lod_2_dist

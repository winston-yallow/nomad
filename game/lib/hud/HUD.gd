extends CanvasLayer


var beacon_mapping := {}
var BeaconVisualizer: PackedScene = preload('BeaconVisualizer.tscn')

export var player := NodePath()
export var enable_game_blur := false
export var color_neutral: Color
export var color_good: Color
export var color_bad: Color
onready var p: Player = get_node(player)

onready var thanks: Control = $BaseControl/Thanks

onready var game_over: ColorRect = $BaseControl/GameOver
onready var charging: ColorRect = $BaseControl/Charging
onready var charging_progress: Label = $BaseControl/Charging/Progress

onready var beacon_base_control: Control = $BaseControl/Beacons
onready var data_label_status: Label = $BaseControl/DataLabels/VBox/Status
onready var data_label_battery: Label = $BaseControl/DataLabels/VBox/Battery
onready var data_label_pollution: Label = $BaseControl/DataLabels/VBox/Pollution
onready var data_label_radiation: Label = $BaseControl/DataLabels/VBox/Radiation

onready var reports: Control = $BaseControl/Reports
onready var report_label_count: Label = $BaseControl/Reports/VBox/Count
onready var report_label_list: Label = $BaseControl/Reports/VBox/List
onready var report_btn_create: Button = $BaseControl/Reports/VBox/BtnCreate


func _ready() -> void:
    
    # warning-ignore:return_value_discarded
    p.connect('beacon_registered', self, 'on_beacon_registered')
    # warning-ignore:return_value_discarded
    p.connect('beacon_unregistered', self, 'on_beacon_unregistered')
    
    # warning-ignore:return_value_discarded
    p.connect('stranded', self, 'on_stranded')
    game_over.visible = false
    
    # warning-ignore:return_value_discarded
    p.connect('charging_started', self, 'on_charging_started')
    # warning-ignore:return_value_discarded
    p.connect('charging_ended', self, 'on_charging_ended')
    charging.visible = false
    
    $BaseControl/GameBlur.visible = enable_game_blur


func _process(delta: float) -> void:
    
    # Update player status
    var status_txt := ''
    match p.current_status:
        Player.STATUS.ROTATE:
            status_txt = 'rotating'
        Player.STATUS.MOVE:
            status_txt = 'moving forward' if p.moving_forward else 'moving backward'
        Player.STATUS.CHARGING:
            status_txt = 'charging...'
        _:
            status_txt = 'waiting...'
    data_label_status.text = 'Status: ' + status_txt
    
    # Update battery level
    data_label_battery.text = 'Battery:          %s' % to_bars(p.battery_level)
    data_label_battery.add_color_override(
        'font_color',
        color_neutral if p.battery_level > 35 else color_bad
    )
    
    # Update air quality
    data_label_pollution.text = 'Air Pollution:    %s' % to_bars(p.pollution)
    data_label_radiation.text = 'Radiation:        %s' % to_bars(p.radiation)
    data_label_pollution.add_color_override(
        'font_color',
        color_neutral if p.pollution > 25 else color_good
    )
    data_label_radiation.add_color_override(
        'font_color',
        color_neutral if p.radiation > 25 else color_good
    )
    
    # Update reports
    report_label_count.text = '%d reports send' % p.reports.size()
    report_label_list.text = 'Reported locations:'
    if p.reports.size() == 0:
        report_label_list.text += '\nnone'
    for i in p.reports:
        report_label_list.text += '\n%d|%d' % [
            round(i.x),
            round(i.z)
        ]
    report_btn_create.disabled = not p.can_report()
    
    # Update charging progress
    if charging.visible:
        var t: float = charging_progress.get_meta('time') + delta
        var battery := round(p.battery_level / 10.0) * 10
        charging_progress.text = '[INFO] charging since %d hours (%d%%)' % [t, battery]
        charging_progress.set_meta('time', t)


func to_bars(percent: float, nr := 6) -> String:
    var bars := percent / (100.0 / float(nr))
    var full_bars := floor(bars)
    var remainder := bars - full_bars
    var half_bars := 0
    if remainder > 0.75:
        full_bars += 1
    elif remainder > 0.25:
        half_bars = 1
    var empty_bars := nr - (full_bars + half_bars)
    # warning-ignore:narrowing_conversion
    # warning-ignore:narrowing_conversion
    return '▓'.repeat(full_bars) + '▒'.repeat(half_bars) + '░'.repeat(empty_bars)


func hide_reports() -> void:
    reports.visible = false


func show_thanks() -> void:
    thanks.visible = true


func on_stranded() -> void:
    game_over.visible = true


func on_charging_started() -> void:
    charging.visible = true
    charging_progress.set_meta('time', 0.0)
    charging_progress.text = '[INFO] charging since 0 hours'


func on_charging_ended() -> void:
    charging.visible = false


func on_report_create_pressed() -> void:
    if p.can_report():
        p.create_report()


func on_exit_pressed() -> void:
    get_tree().quit()


func on_abort_charging_pressed() -> void:
    p.abort_charging()


func on_beacon_registered(beacon: Beacon) -> void:
    if not beacon in beacon_mapping:
        beacon_mapping[beacon] = BeaconVisualizer.instance()
        beacon_mapping[beacon].beacon = beacon
        beacon_mapping[beacon].p = p
        beacon_base_control.add_child(beacon_mapping[beacon])


func on_beacon_unregistered(beacon: Beacon) -> void:
    if beacon in beacon_mapping:
        beacon_mapping[beacon].delete()
        # warning-ignore:return_value_discarded
        beacon_mapping.erase(beacon)

extends Control


var beacon: Beacon
var p: Player

var fade_speed := 3.0
var should_delete := false
var should_hide := false

var last_cmds := []
onready var commands: VBoxContainer = $Backdrop/Commands


func _ready() -> void:
    $Backdrop/Name.text = beacon.title
    $Backdrop/Description.text = beacon.description
    modulate.a = 0.0


func _process(delta: float) -> void:
    
    if $Backdrop/Name.text != beacon.title:
        $Backdrop/Name.text = beacon.title
    if $Backdrop/Description.text != beacon.description:
        $Backdrop/Description.text = beacon.description
    
    if should_delete or (should_hide and modulate.a > 0.35):
        modulate.a -= delta * fade_speed
        if should_delete and modulate.a <= 0.0:
            queue_free()
    elif modulate.a < 1.0:
        modulate.a = min(modulate.a + (delta * fade_speed), 1.0)
    
    # Update position
    var vp := get_viewport()
    var cam := vp.get_camera()
    if not cam.is_position_behind(beacon.global_transform.origin):
        rect_position = cam.unproject_position(beacon.global_transform.origin)
        visible = beacon.visible
        should_hide = rect_position.y > (vp.size.y - 200)
    else:
        visible = false
        should_hide = false
    
    # Update list of command buttons if it changed
    if last_cmds != beacon.commands:
        last_cmds = beacon.commands.duplicate()  # Needs duplication to detect changes
        
        # Remove old command buttons
        for child in commands.get_children():
            child.queue_free()
        
        # Create new command buttons
        for cmd in beacon.commands:
            var btn := Button.new()
            btn.text = cmd.name
            btn.disabled = p.distance_to(beacon) > cmd.max_distance
            btn.set_meta('max_distance', cmd.max_distance)
            # warning-ignore:return_value_discarded
            btn.connect('pressed', cmd, 'execute', [p])
            commands.add_child(btn)
        
    # Update button visibility even if the list of commands did not change
    else:
        for btn in commands.get_children():
            btn.disabled = p.distance_to(beacon) > btn.get_meta('max_distance')


func delete():
    should_delete = true


func is_deleting() -> bool:
    return should_delete

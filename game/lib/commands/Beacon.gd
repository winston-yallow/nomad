class_name Beacon
extends Area


var commands := []

export var title := ''
export(String, MULTILINE) var description := ''

# This is only so we can set a default mask value for this class_named script
export(int, LAYERS_3D_PHYSICS) var detection_mask := 0b10


func _ready() -> void:
    
    collision_layer = 0b0
    collision_mask = detection_mask
    
    # warning-ignore:return_value_discarded
    connect('body_entered', self, 'on_body_entered')
    # warning-ignore:return_value_discarded
    connect('body_exited', self, 'on_body_exited')


func register_cmd(cmd: Command) -> void:
    if not cmd in commands:
        commands.append(cmd)


func unregister_cmd(cmd: Command) -> void:
    while cmd in commands:
        commands.erase(cmd)


func clear_cmds() -> void:
    # warning-ignore:return_value_discarded
    commands.clear()


func on_body_entered(other: PhysicsBody) -> void:
    if other.has_method('register_beacon'):
        other.register_beacon(self)


func on_body_exited(other: PhysicsBody) -> void:
    if other.has_method('unregister_beacon'):
        other.unregister_beacon(self)

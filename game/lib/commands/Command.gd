class_name Command
extends Reference


var name: String
var fn: FuncRef
var max_distance: float
var additional_args: Array


func _init(
        display_name: String,
        object: Object,
        method: String,
        distance: float,
        arguments := []
    ) -> void:
    
    name = display_name
    fn = funcref(object, method)
    max_distance = distance
    additional_args = arguments.duplicate()


func execute(executor: Object):
    fn.call_funcv([executor] + additional_args)

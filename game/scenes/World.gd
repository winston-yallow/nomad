extends Spatial


var names := [
    'Vesper',
    'Liam',
    'Riley',
    'Saren',
    'Lucas',
    'Luke',
    'Ayne',
    'Ethan',
    'Owen',
    'Zoey',
    'Rael',
    'Scar',
    'Carter',
    'Aven',
    'Luis',
    'Ellie',
    'Drew',
    'Chloe'
]

var rng := RandomNumberGenerator.new()


func _ready() -> void:
    rng.randomize()


func rename_robots() -> void:
    var robots := get_tree().get_nodes_in_group('companions')
    for bot in robots:
        bot.rename(pop_random_name(), 'human (emulated)')

func pop_random_name() -> String:
    return names[rng.randi_range(0, names.size() - 1)]

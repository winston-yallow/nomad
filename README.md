# The Last Nomad

Small godot game made for the [Godot Wild Jam #29](https://itch.io/jam/godot-wild-jam-29).

Builds available at [winston-yallow.itch.io/the-last-nomad](https://winston-yallow.itch.io/the-last-nomad).

## Setup

### Git Clone with Submodules

This repo contains the cutscene addon as a git submodule. Therefore you will need to clone it recursively:
```bash
git clone --recurse-submodules https://gitlab.com/winston-yallow/nomad.git
```

### Import Godot Project

Add the project by clicking "Import" in the Project List dialogue of godot.
Select the `project.godot` file in the `game` directory of this repo.

## Warning

This was made within one week for a jam. The code is written fast, not elegant or maintainable.
Beware of the bugs! I am sure there are plenty as there was not time to playtest this.

## License

The code for this game is available under the MIT License.

The textures are from [cc0textures.com/](https://cc0textures.com/) and available under the CC0 License.

Audio is based on sound effects from a Sonniss GDC Game Audio Bundle.
